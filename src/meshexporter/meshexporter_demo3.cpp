/*
**	meshexporter_demo3.cpp
**
**	This is part of Shiokaze, a research-oriented fluid solver for computer graphics.
**	Created by Ryoichi Ando <rand@nii.ac.jp> on Aug 29, 2017. 
**
**	Permission is hereby granted, free of charge, to any person obtaining a copy of
**	this software and associated documentation files (the "Software"), to deal in
**	the Software without restriction, including without limitation the rights to use,
**	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
**	Software, and to permit persons to whom the Software is furnished to do so,
**	subject to the following conditions:
**
**	The above copyright notice and this permission notice shall be included in all copies
**	or substantial portions of the Software.
**
**	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
**	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
**	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
**	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
**	CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
**	OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
//
#include <shiokaze/meshexporter/meshexporter3_interface.h>
#include <shiokaze/cellmesher/cellmesher3_interface.h>
#include <shiokaze/core/timer.h>
#include <shiokaze/core/console.h>
#include <shiokaze/core/runnable.h>
//
SHKZ_USING_NAMESPACE
//
class meshexporter_demo3 : public runnable {
public:
	LONG_NAME("Mesh Exporter Demo")
	//
	meshexporter_demo3() {
		export_path = "sphere.ply";
	}
	//
protected:
	//
	virtual void load( configuration &config ) override {
		if( console::get_root_path().size()) {
			export_path = console::get_root_path() + "/" + export_path;
		}
	}
	//
	virtual void configure( configuration &config ) override {
		//
		config.get_unsigned("Resolution",shape[0], "Grid resolution");
		shape[1] = shape[2] = shape[0];
		config.get_unsigned("ResolutionX",shape[0],"Resolution towards X axis");
		config.get_unsigned("ResolutionY",shape[1],"Resolution towards Y axis");
		config.get_unsigned("ResolutionZ",shape[2],"Resolution towards Z axis");
		//
		config.get_string("Path",export_path,"PLY export path");
		dx = shape.dx();
	}
	//
	virtual void initialize( const environment_map &environment ) override {
		//
		scoped_timer timer(this);
		//
		timer.tick(); console::dump("Generating spherical levelset (%dx%dx%d)...", shape[0],shape[1],shape[2]);
		array3<double> sphere(shape);
		const auto shape = sphere.shape();
		sphere.parallel_all([&](int i, int j, int k, auto &it ) {
			vec3d p = vec3d(i-shape.w/2.0,j-shape.h/2.0,k-shape.d/2.0) * dx;
			it.set(p.len() - 0.4);
		});
		console::dump( "Done. Took %s.\n", timer.cstock());
		//
		std::vector<vec3d> vertices;
		std::vector<std::vector<size_t> > faces;
		//
		timer.tick(); console::dump("Generating mesh...");
		mesher->generate_mesh(sphere,vertices,faces);
		console::dump( "Done. Generated %d vertices and %d faces. Took %s.\n", vertices.size(),faces.size(),timer.cstock());
		//
		timer.tick(); console::dump("Exporting mesh \"%s\"...",export_path.c_str());
		meshexporter3_driver exporter{"meshexporter"};
		exporter->set_mesh(vertices,faces);
		exporter->export_ply(export_path.c_str());
		console::dump( "Done. Took %s.\n", timer.cstock());
		//
	};
	//
private:
	//
	shape3 shape{64,64,64};
	double dx;
	std::string export_path;
	cellmesher3_driver mesher{this,"marchingcubes"};
};
//
extern "C" module * createInstance() {
	return new meshexporter_demo3;
}
//
extern "C" const char *license() {
	return "MIT";
}
