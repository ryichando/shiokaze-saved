//! [code]
#include <shiokaze/my_module/my_module_interface.h>
#include <shiokaze/core/console.h>
//
SHKZ_USING_NAMESPACE
//
class my_module_implementation1 : public my_module_interface {
private:
	//
	LONG_NAME("My Module Example")
	ARGUMENT_NAME("my_module")
	//
	void load( configuration &config ) override {
		console::dump("load called.\n");
	}
	void configure( configuration &config ) override {
		console::dump("configure called.\n");
	}
	unsigned add_two_integers( unsigned a, unsigned b ) const override {
		return a+b;
	}
	void initialize( const unsigned &param1, const unsigned &param2 ) override {
		console::dump("initialize called. param1 = %d, param2 = %d\n", param1, param2);
	}
};
extern "C" module * createInstance() {
	return new my_module_implementation1;
}
extern "C" const char *license() {
	return "MIT";
}
//! [code]