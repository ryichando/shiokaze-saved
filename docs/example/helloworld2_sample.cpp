//! [demo]
#include <shiokaze/core/console.h>
#include <shiokaze/array/shared_array2.h>
#include <shiokaze/ui/drawable.h>
#include <shiokaze/math/vec.h>
//
SHKZ_BEGIN_NAMESPACE
//
class helloworld2 : public drawable {
private:
	//
	//! [defines]
	LONG_NAME("Hello World 2D")
	ARGUMENT_NAME("helloworld2")
	//! [defines]
	//
	//! [setup_window]
	virtual void setup_window( std::string &name, int &width, int &height ) const override {
		name = "helloworld";
	}
	//! [setup_window]
	virtual void configure( configuration &config ) override {
		console::dump( "Configuring helloworld...\n");
	}
	//! [initialize]
	virtual void initialize() override {
		console::dump( "Initializing helloworld...\n");
		shared_array2<double> array(shape2(64,64));
		array->parallel_all([&]( int i, int j, auto &it ) {
			it.set(i+j);
		});
	}
	//! [initialize]
	//! [keyboard]
	virtual bool keyboard ( char key ) override {
		console::dump( "Keyboard %c\n", key );
		return drawable::keyboard(key);
	}
	//! [keyboard]
	virtual void idle() override {}
	//! [cursor]
	virtual void cursor( int width, int height, double x, double y ) override {
		mouse_pos = vec2d(x,y);
	}
	//! [cursor]
	//! [mouse]
	virtual void mouse( int width, int height, double x, double y, int button, int action ) override {
		console::dump( "Mouse (x,y) = (%.2f,%.2f), (%d), (%d)\n", x, y, button, action );
	}
	//! [mouse]
	//! [draw]
	virtual void draw( const graphics_engine &g, int width, int height ) const override {
		g.color4(1.0,1.0,1.0,1.0);
		g.draw_string(vec2d(0.025,0.025).v, "This is a helloworld window");
		g.point_size(2.0);
		g.begin(graphics_engine::MODE::POINTS);
		g.vertex2v(mouse_pos.v);
		g.end();
		g.point_size(1.0);
	}
	//! [draw]
	vec2d mouse_pos;
};
//
extern "C" module * createInstance() {
	return new helloworld2;
}
extern "C" const char *license() {
	return "MIT";
}
//
SHKZ_END_NAMESPACE
//! [demo]