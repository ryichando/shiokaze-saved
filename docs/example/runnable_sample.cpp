//! [code]
#include <shiokaze/core/runnable.h>
#include <shiokaze/core/console.h>
//
SHKZ_USING_NAMESPACE
//
class runnable_demo : public runnable {
private:
	//
	LONG_NAME("Runnable Demo")
	ARGUMENT_NAME("RunnableDemo")
	//
	virtual void load( configuration &config ) override {
		config.get_unsigned("Param1",param_on_load);
	}
	virtual void configure( configuration &config ) override {
		config.get_unsigned("Param2",param_on_configure);
	}
	virtual void run_onetime() override {
		console::dump("Param1 = %d, Param2 = %d.\n", param_on_load, param_on_configure);
	};
	unsigned param_on_load {0};
	unsigned param_on_configure {0};
};
extern "C" module * createInstance() {
	return new runnable_demo;
}
extern "C" const char *license() {
	return "MIT";
}
//! [code]
//! [code2]
#include <shiokaze/core/runnable.h>
#include <shiokaze/core/console.h>
#include <shiokaze/my_module/my_module_interface.h>
//
SHKZ_USING_NAMESPACE
//
class runnable_demo : public runnable {
private:
	//
	LONG_NAME("Runnable Demo")
	ARGUMENT_NAME("RunnableDemo")
	//
	virtual void load( configuration &config ) override {
		config.get_unsigned("Param1",param_on_load);
		set_environment("param0",&param_on_load);
	}
	virtual void configure( configuration &config ) override {
		config.get_unsigned("Param2",param_on_configure);
		set_environment("param1",&param_on_configure);
	}
	virtual void run_onetime() override {
		console::dump("Param1 = %d, Param2 = %d.\n", param_on_load, param_on_configure);
		unsigned result = my_module->add_two_integers(param_on_load,param_on_configure);
		console::dump("Result = %d\n", result );
	};
	unsigned param_on_load {0};
	unsigned param_on_configure {0};
	//
	my_module_driver my_module{this,"my_module_implementation1"};
};
extern "C" module * createInstance() {
	return new runnable_demo;
}
extern "C" const char *license() {
	return "MIT";
}
//! [code2]
//! [output]
--------------------  Shiokaze Core (MIT)  ---------------------  
	 A research-oriented fluid solver for computer graphics     
 Designed and maintained by Ryoichi Ando <rand@nii.ac.jp>  
----------------------------------------------------------------
   Arguments: Target=my_runnable 
   Date = 2018-Aug-26 06:53:27 UTC
   CPU = Intel(R) Core(TM) i7-7920HQ CPU @ 3.10GHz
   Clang version = 4.2.1
   Build target = Release
   Available cores = 8
---------------------  Loading Simulation  ---------------------  
✔ Loaded "my_own_project/my_runnable.dylib" (MIT)
✔ Loaded "public/sysstats.dylib" (MIT)
--------------------  User Interface (UI)  ---------------------  
   Log = ""
   RemoteDeleteTemplate = "rm -rf %s"
   SSHPath = ""
   SysStats = "sysstats"
------------------------  Root (Root)  -------------------------  
●  Target = "my_runnable"
----------------  Runnable Demo (RunnableDemo)  ----------------  
   Param1 = 0
   Param2 = 0
--------------  System Stats Analyzer (SysStats)  --------------  
   PlotTemplate = ""
----------------------------------------------------------------
Param1 = 0, Param2 = 0.
Arguments: Target=my_runnable 
✘ Unload scheduled "my_own_project/my_runnable.dylib"
✘ Unload scheduled "public/sysstats.dylib"
----------------------------------------------------------------
//! [output]
//! [output2]
--------------------  Shiokaze Core (MIT)  ---------------------  
     A research-oriented fluid solver for computer graphics     
 Designed and maintained by Ryoichi Ando <rand@nii.ac.jp>  
----------------------------------------------------------------
   Arguments: Target=my_runnable Param2=3 Param1=4 
   Date = 2018-Aug-26 07:07:48 UTC
   CPU = Intel(R) Core(TM) i7-7920HQ CPU @ 3.10GHz
   Clang version = 4.2.1
   Build target = Release
   Available cores = 8
---------------------  Loading Simulation  ---------------------  
✔ Loaded "my_own_project/my_runnable.dylib" (MIT)
✔ Loaded "my_own_project/my_module_implementation1.dylib" (MIT)
load called.
✔ Loaded "public/sysstats.dylib" (MIT)
configure called.
--------------------  User Interface (UI)  ---------------------  
   Log = ""
   RemoteDeleteTemplate = "rm -rf %s"
   SSHPath = ""
   SysStats = "sysstats"
------------------------  Root (Root)  -------------------------  
●  Target = "my_runnable"
----------------  Runnable Demo (RunnableDemo)  ----------------  
   MyModule = "my_module_implementation1"
●  Param1 = 4
●  Param2 = 3
--------------  System Stats Analyzer (SysStats)  --------------  
   PlotTemplate = ""
----------------------------------------------------------------
Param1 = 4, Param2 = 3.
Result = 7
initialize called. param1 = 4, param2 = 3
Arguments: Target=my_runnable Param2=3 Param1=4 
✘ Unload scheduled "my_own_project/my_module_implementation1.dylib"
✘ Unload scheduled "my_own_project/my_runnable.dylib"
✘ Unload scheduled "public/sysstats.dylib"
----------------------------------------------------------------
//! [output2]