//! [code]
#ifndef SHKZ_MY_MODULE_INTERFACE_H
#define SHKZ_MY_MODULE_INTERFACE_H
//
#include <shiokaze/core/recursive_configurable_module.h>
//
SHKZ_BEGIN_NAMESPACE
//
class my_module_interface : public recursive_configurable_module {
public:
	//
	DEFINE_MODULE(my_module_interface,"My Module","MyModule","My module for demonstration")
	virtual unsigned add_two_integers( unsigned a, unsigned b ) const = 0;
	//
private:
	//
	virtual void initialize( const unsigned &param0, const unsigned &param1 ) = 0;
	virtual void initialize( const configurable::environment_map &environment ) override {
		//
		assert(check_set(environment,{"param0","param1"}));
		initialize(
			get_env<unsigned>(environment,"param0"),
			get_env<unsigned>(environment,"param1")
		);
	}
};
//
using my_module_ptr = std::unique_ptr<my_module_interface>;
using my_module_driver = recursive_configurable_driver<my_module_interface>;
//
SHKZ_END_NAMESPACE
//
#endif
//! [code]