//! [argument_define1]
unsigned value (default_value);
config.get_unsigned("ArgumentItem",value,"Parameter Description");
//! [argument_define1]

//! [argument_define2]
unsigned value;
config.set_default_unsigned("ArgumentItem",default_value);
config.get_unsigned("ArgumentItem",value,"Parameter Description");
//! [argument_define2]