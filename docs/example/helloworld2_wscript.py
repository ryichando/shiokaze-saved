#! [demo]
def build(bld):
	bld.shlib(source = 'helloworld2.cpp',
			target = bld.get_target_name(bld,'helloworld2'),
			cxxflags = ['-DSPATIAL_DIM=2'],
			use = bld.get_target_name(bld,'core'))
#! [demo]