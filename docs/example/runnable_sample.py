#! [code]
def build(bld):
	bld.shlib(source = 'my_runnable.cpp',
			target = bld.get_target_name(bld,'my_runnable'),
			use = bld.get_target_name(bld,['core']))
#! [code]