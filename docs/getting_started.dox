/**

\~english

\page getting_started Getting Started

\section demo_flow Demo Overview

\subsection build_setting Build Setting

Build setting file is placed at src/sub_project_name/wscript. This example places at src/helloworld/wscript. Contents are following:

\snippet helloworld2_wscript.py demo

Source code is specified by source = 'helloworld2.cpp'. If you want to specify multiple sources, you may add them as array such that source = ['src1.cpp','src2.cpp']. Any drawable instance needs to know spatial dimension, and this is provided as -DSPATIAL_DIM=2 for the case of two dimensions. For three dimensions, change to -DSPATIAL_DIM=3.

\subsection source_file Source Code

All the source files should be placed in src/sub_project_name/. This examples places helloworld2.cpp at src/helloworld/helloworld2.cpp. Contents of the source file are below:

\snippet helloworld2_sample.cpp demo

In Shiokaze, any project with UI should define a class that inherits drawable class. In a source file, it should define a function: extern "C" module * createInstance() that instanciates the class and returns pointer to it. The class also should define the full name and the argument name used in command line interface using the macros:

\snippet helloworld2_sample.cpp defines

Now, let's take a deeper look what is going on in each function. We start with setup_window() function:

\snippet helloworld2_sample.cpp setup_window

setup_window is where you dictate the window title, width and the height. In this example, we simply set the window title as "helloworld". On the other hand, width and height are not explicitly specified. In this case, window size will be set arbitrary by the host program. Next, we will move to initialize() function.

\snippet helloworld2_sample.cpp initialize

In this function we first output a text "Initializing helloworld...". What is different from printf() is that dump() also writes into a log file, and it increments indent if it includes ">>>" and decrement if includes "<<<". The line of shared_array2<double> array(shape2(64,64)); generates a 64 x 64 grid of the type of double. array->parallel_all() processes each cell in parallel, and set their values as i+j, where i denotes index position on x coordinate and j position on y coordinate. shared_array2 means arrays shared in the whole program, and the allocated grids still remain in a pool after they leaves a scope. If an array of the same type is requested elsewhere, an array in the pool will be re-used. Next, we move to the function keyboard.

\snippet helloworld2_sample.cpp keyboard

keyboard function is called whenever a key is pressed. char key tells what kind of key is pressed. For example, when A key is pressed, 'A' will be assgined. The function returns \c true if the key event is processed. As default, drawable class handles key event as follows: when '/' is pressed, it pauses or resumes simulation. If 'R' is pressed, it calls initialize() function. If '.' is pressed, when a simulation is paused, it calls idle() once. Next, we explain idle function. Function idle corresponds to simulation loop, and is repeatedly called while a simulation is running. Here, we just leave blank because we do not do anything here. A simulation pauses and therefore idle function will not be called if either a member variable of runnable class m_running is set \c false or set_running(false) is called. Contrarily, if m_running is set \c true or set_running(true) is called, the simulation resumes and idle function will again be called. Next, we will detail mouse function.

\snippet helloworld2_sample.cpp mouse

Arguemnt width and height are the window width and height. Arguments x and y corresponds to mouse position, and button and action correspond to the type of mouse click and the mouse button number. Here, we use dump() to examine the actual values of arguments. Finally, we explain draw function.

\snippet helloworld2_sample.cpp draw

Function draw takes in charge of drawing tasks through the instance of graphics_engine class, which is practically a wrapper class for OpenGL - it provides various corresponding APIs for OpenGL. This example simply draws a dot of size 2 at the position of mouse cursor.

\subsection running_demo Running the Demo

You can run this demo by

~~~{.sh}
./waf
./run Target=helloworld2
~~~

\~japanese

\page getting_started ことはじめ

\section demo_flow デモの解説

\subsection build_setting ビルド設定ファイル

ビルド設定ファイルは、src/サブプロジェクト名/wscript に記述します。この例では、src/helloworld/wscript に記述されています。内容は次の通りです。

\snippet helloworld2_wscript.py demo

source = 'helloworld2.cpp' で、コンパイルするソースファイルを指定します。複数ある場合は、source = ['src1.cpp','src2.cpp'] のように、配列で与えます。-DSPATIAL_DIM=2 は drawable クラスに2次元のシミュレーションであることを明示するための定義です。3次元の場合、-DSPATIAL_DIM=3 に変更します。

\subsection source_file ソースファイル

ソースファイルは、src/サブプロジェクト名/ の中に記述します。この例では、src/helloworld/helloworld2.cpp に記述されています。内容は次の通りです。

\snippet helloworld2_sample.cpp demo

潮風では、UIを備えたプロジェクトを設計する場合、drawable クラスを継承した新しいクラスを定義することから始まります。そして、ソースファイルの最後に extern "C" module * createInstance() を定義します。この関数内で、新しいクラスのインスタンスを生成し、そのポインタを返します。クラスには、次のマクロを使って、クラスのフルネーム(Long name)とコマンドラインで参照する時の名前(Argument name)を設定します。

\snippet helloworld2_sample.cpp defines

それぞれの関数で行っている処理については、まず setup_window() 関数内から解説します。

\snippet helloworld2_sample.cpp setup_window

setup_window では、ウィンドウのタイトルや大きさを設定します。ここでは、ウィンドウのタイトルを helloworld としています。width と height は明示的に設定していません。この時、ウィンドウの大きさは適当な大きさとなります。次に、initialize() 関数内について解説します。

\snippet helloworld2_sample.cpp initialize

initialize 関数は、コードがこのコードでは、最初に "Initializing helloworld..." という文字列を出力します。printf() 関数と異なるのは、dump() ではログファイルにも同時に記録される点と、出力に ">>>" が含まれると、行を一度インデントし、"<<<" が含まれると、行のインデントをひとつ解消する点です。
shared_array2<double> array(shape2(64,64)); では、double 型の大きさが 64 x 64 の格子を一つ作成します。そして、array->parallel_all() で格子の値を並列に処理し、それぞれ値を i+j に設定しています。
ここで、i は格子の x 座標系のインデックス位置、j は格子の y 座標系のインデックス位置を指します。shared_array2 とは、共有配列のことで、ここで作成された配列はスコープを抜けても解放されません。使用されなくなった shared_array2 のインスタンスはいったんプールに貯蔵されます。もし同じ種類の shared_array2 のインスタンスが他の場所で再度要求された時、再利用されます。次に、keyboard 関数内について説明します。

\snippet helloworld2_sample.cpp keyboard

keyboard 関数では、キーが押された時呼び出されます。char key はキーで、例えば A キーが押されたとき 'A' が渡されます。もしキーボードの処理を実際に行った場合、\c true を返します。drawable では、デフォルトのキーボード処理として、'/' が押されたらシミュレーションを一時停止したり実行したりします。'R' が押されたら、initialize() 関数を呼びます。'.' キーが押されたら、シミュレーションが一時停止の時、一度だけ idle 関数を呼びます。
次に、idle 関数について説明します。idle 関数はシミュレーションのループに匹敵し、シミュレーションが実行されている間は常に呼び出され続けます。ここでは何もしないので、空となっています。runnable のメンバ関数の m_running を \c false にするか、set_running(false) を呼ぶと、シミュレーションは一時停止され、idle 関数は呼び出されなくなります。反対に、m_running を \c true にするか、set_running(true) を呼ぶと、シミュレーションは再開され、idle 関数が呼ばれるようになります。次に、mouse 関数について説明します。

\snippet helloworld2_sample.cpp mouse

mouse 関数の引数で渡される width と height はウィンドウの横幅と縦幅となります。x と y はマウスの座標、button はボタンの番号、action はクリックの種類となります。それぞれ、dump() 関数を使ってどのような値が入っているか出力しています。最後に、draw 関数について解説します。

\snippet helloworld2_sample.cpp draw

draw 関数では、graphics_engine のインスタンスを通して、描画処理を行います。graphics_engine は実質 OpenGL のラッパー関数で、各種 OpenGL の API に対応する関数が提供されています。この例では、マウスの位置に大きさが2の点を描いています。

\subsection running_demo デモを実行する

このデモは、次のコマンドラインで実行できます。

~~~{.sh}
./waf
./run Target=helloworld2
~~~

*/