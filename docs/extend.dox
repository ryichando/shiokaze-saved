/**

\~english

\page extend_by_your_own Extending By Your Own

\section recursive_configurable_principle Principle

\section define_parameters Defining Parameters

When designing your own project, you wish to define your own parameters. Parameters can be defined twofold. If you inherit the class "recursive_configurable", you can define parameters either in function recursive_configurable::load or recursive_configurable::configure such that

\snippet define_params.cpp argument_define1

where default_value is the default parameter value. Alternatively, you can do the same by

\snippet define_params.cpp argument_define2

\~japanese

\page extend_by_your_own 独自の拡張をする

潮風は、\ref recursive_configurable クラスを継承した新しいクラスを定義して、機能を拡張します。例外として、UIを持つ機能を実装する場合は \ref drawable クラスを、単独の実行を目的とする場合は \ref runnable クラスを継承したクラスを定義します。そして、extern "C" module * createInstance() でそれらの定義したクラスの新しいインスタンスを返す関数を定義します。以下に、その詳細を解説します。

\section extend_run 実行部を拡張する

まず、my_runnable のビルドファイル src/my_runnable/wscript を作成し、次のようにコンテンツを書きます。

\snippet runnable_sample.py code

そして、src/my_runnable/my_runnable.cpp を作成して、次のようにコンテンツを書きます。

\snippet runnable_sample.cpp code

最後に、コンパイルして実行します。

~~~{.sh}
./waf
./run Target=my_runnable
~~~

次のような結果がターミナルに出力されることを確認します。

\snippet runnable_sample.cpp output

\subsection 実行部の解説

\ref runnable のインスタンスは、最初にコンストラクタが呼ばれ、次に load、configure という順番にルーチンが呼ばれます。load は、主にリソースや他の動的ライブラリに読み込みを想定し、configure は各種パラメータの設定・調整を想定しています。run_onetime は、load と configure が呼ばれた後に一度だけ呼ばれます。config.get_unsigned() では、それぞれ load が呼ばれる時と configure が呼ばれる時に、パラメータの値を取得しています。これらパラメータは、

~~~{.sh}
./run Target=my_runnable Param1=10 Param2=20
~~~

のように、コマンドラインから与えられます。

\section module_extension モジュールで拡張をする

潮風では、モジュールという単位で機能を拡張できます。本章では、新しいモジュールを作成して、それを my_runnable から使用する例を解説します。モジュールは、インターフェース宣言部と実装部から成り立っています。まず、インターフェース宣言部のファイルについて解説します。インターフェース宣言は、include/shiokaze/my_module の中で行います。このために、まず次のコマンドを走らせて my_module のディレクトリを作成し、その中に my_module_interface.h というファイルを作成します。

~~~{.sh}
mkdir include/shiokaze/my_module
touch include/shiokaze/my_module/my_module_interface.h
~~~

そして、my_module_interface.h のコンテンツを次のように書きます。

\snippet my_module_sample.h code

インターフェース宣言部では、実装について何も行っていない点に注目して下さい。次に、src/my_module というディレクトリを作成し、そこに my_module_implementation1.cpp というファイルを作成し、コンテンツを次のように書きます。

\snippet my_module_sample.cpp code

次に、同ディレクトリに wscript を作成し、コンテンツを次のように書きます。

\snippet my_module_sample.py code

これで、my_module の実装は終わりです。試しに、次のコマンドを走らせて、shiokaze-main に移動し、コンパイルが成功するか確認します。

~~~{.sh}
./waf
~~~

成功したら、最後に my_module を my_runnable から呼び出してみます。src/my_runnable/my_runnable.cpp を次のように書き換えます。

\snippet runnable_sample.cpp code2

再度コンパイルし、プログラムを実行します。

~~~{.sh}
./waf
./run Target=my_runnable Param1=4 Param2=3
~~~

次のような結果がターミナルに出力されれば成功です。

\snippet runnable_sample.cpp output2

\section define_parameters パラメータを定義する

自分独自のプロジェクトを設計する時、独自のパラメータを定義したいと思うようになります。パラメータは次の二通りで定義されます。もしクラス "recursive_configurable" を継承した場合、パラメータは recursive_configurable::load あるいは recursive_configurable::configure のルーチン内でこのように定義されます。

\snippet define_params.cpp argument_define1

ここで default_value は初期値です。同じことが、次のコードでも可能です。

\snippet define_params.cpp argument_define2

*/